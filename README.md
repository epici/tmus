# tmus

[Discord bot](https://discordapp.com/developers/docs/intro) for
[Fantastic Contraption](http://www.fantasticcontraption.com/original/) competitions,
written in [Python](https://www.python.org)
using [discord.py](https://pypi.org/project/discord.py/).

This bot is meant to make hosting competitions easier,
and secondarily also make participating in competitions easier,
even if the rules are complicated.
The bot handles most of the work in running a competition.

The name was decided by a community vote.
Tmus is a word in the Fantastic Contraption community,
it means abusing the incremental ID system
to look for a design or level.
Starting with a known ID,
you count up or down looking for something,
that is tmusing.
Since it can be used to discover
levels for later rounds
or other people's solutions to a level,
it is a form of cheating in competitions.
The word originates from tmusquiez,
a player who used this technique.