"""
Utilities for working with data structures.
"""

import io_utils
import numpy as np

class dummy(io_utils.auto_repr):
    """
    Dummy class for generic data storage.
    """
    def __init__(self, **properties):
        """
        Set instance variables from keyword arguments.
        """
        self.__dict__.update(properties)

class changed(io_utils.auto_repr):
    """
    Class which stores only changes rather than the full object.
    Changes to the original may be reflected in this.
    """
    def __init__(self, original, target=None, ignore_hidden=True):
        """
        Initialize as a view of some object.
        If a target is provided, sets all properties which are different.
        """
        changes = {}
        object.__setattr__(self, 'original', original)
        object.__setattr__(self, 'changes', {})
        object.__setattr__(self, 'deleted', set())
        if target:
            for key in dir(target):
                if ignore_hidden and key.startswith('_'):continue
                new_value = getattr(target, key)
                if not hasattr(original, key) or np.any(getattr(original, key) != new_value):
                    changes[key] = new_value
    def apply(self, *args, **kwargs):
        """
        Applies these changes to the original, without modifying this object.
        Uses the set_attributes function. Arguments go there.
        """
        set_attributes(self.original, self.changes, *args, **kwargs)
    def __setattr__(self, name, value):
        if name in self.__dict__:
            object.__setattr__(self, name, value)
        else:
            changes = self.changes
            deleted = self.deleted
            if name in deleted:
                deleted.remove(name)
            changes[name] = value
    def __getattr__(self, name):
        deleted = self.deleted
        if name in deleted:
            raise AttributeError('The attribute '+repr(name)+' has been deleted')
        changes = self.changes
        if name in changes:
            return changes[name]
        original = self.original
        return getattr(original, name)
    def __delattr__(self, name):
        if name in self.__dict__:
            del self.__dict__[name]
        else:
            changes = self.changes
            deleted = self.deleted
            if name in changes:
                del changes[name]
            deleted.add(name)
    def __dir__(self):
        result = set(dir(self.original))
        result -= set(self.deleted)
        result |= set(self.changes)
        return result

def set_attributes(obj, changes:dict, policy:dict=None):
    """
    Set an object's attributes from a dict.
    Policy string can be:
        replace
        if_exists
        if_not_exists
        if_none
        if_not_none
        if_true
        if_false
    Provide fallbacks separated by spaces.
    Policy function will take 3 arguments, object, name, and new value.
    
    Args:
        obj: object to modify
        changes: dict which maps attribute names to the new value
        policy: dict which maps attribute names to string or combining function
    """
    policy = policy or {}
    for key, value in changes.items():
        mode = policy.get(key, None) or 'replace'
        submodes = None
        try:
            submodes = mode.split()
        except:
            mode(obj, key, value)
        if submodes:
            for mode in submodes:
                if mode == 'replace':
                    setattr(obj, key, value)
                    break
                elif mode == 'if_exists':
                    if hasattr(obj, key):
                        setattr(obj, key, value)
                        break
                elif mode == 'if_not_exists':
                    if not hasattr(obj, key):
                        setattr(obj, key, value)
                        break
                elif mode == 'if_none':
                    if hasattr(obj, key) and getattr(obj, key) is None:
                        setattr(obj, key, value)
                        break
                elif mode == 'if_not_none':
                    if hasattr(obj, key) and getattr(obj, key) is not None:
                        setattr(obj, key, value)
                        break
                elif mode == 'if_true':
                    if hasattr(obj, key) and getattr(obj, key):
                        setattr(obj, key, value)
                        break
                elif mode == 'if_false':
                    if hasattr(obj, key) and not getattr(obj, key):
                        setattr(obj, key, value)
                        break

def set_all(coll, value, keys):
    """
    Set all keys to map to this value in coll.
    """
    for key in keys:
        coll[key] = value