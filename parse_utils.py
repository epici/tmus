"""
Utilities for parsing text,
and the base class for parsers.
"""

import re
import ast
import copy
import functools
import numpy as np

import data_utils

regex_literal = re.compile('"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"')
regex_whitespace = re.compile('\\s+')
def tokens(istr):
    """
    If this string was command line arguments,
    what would the arguments be?
    Words are kept as words.
    String literals become their evaluated form.
    """
    end = len(istr)
    mark = 0
    while mark<end:
        if istr[mark]=='"':
            match = regex_literal.search(istr, mark)
            if not match:
                raise ValueError('Could not find a closing quote in the text')
            to = match.end()
            yield ast.literal_eval(istr[mark:to])
            mark = to
            if mark<end:
                match = regex_whitespace.search(istr, mark)
                if not match or match.start() != mark:
                    raise ValueError('Separator must be a space')
                mark = match.end()
        else:
            match = regex_whitespace.search(istr, mark)
            if match:
                to = match.start()
            else:
                to = end
            yield istr[mark:to]
            mark = match.end() if match else to

class handler(object):
    """
    Base class for command handlers.
    In all methods, context will be the first argument.
    """
    async def process(self, context, tokens, sub, start, finish):
        """
        Process a list of tokens.
        Uses sub to try and get subhandlers.
        Uses start at the start.
        Uses finish at the end.
        """
        await start()
        before = []
        after = []
        last = None
        for token in tokens:
            isub = await sub(token)
            if isub:
                if last:
                    icontext = data_utils.changed(context)
                    icontext.before = np.asarray(before, dtype=str)
                    icontext.after = np.asarray(after, dtype=str)
                    icontext.parent = self
                    before, after = await last(icontext), []
                    icontext.apply({
                        'result':'if_false',
                        })
                last = isub
            else:
                if last:
                    after.append(token)
                else:
                    before.append(token)
        if last:
            icontext = data_utils.changed(context)
            icontext.before = np.asarray(before, dtype=str)
            icontext.after = np.asarray(after, dtype=str)
            icontext.parent = self
            before, after = await last(icontext), []
            icontext.apply({
                'result':'if_false',
                })
        return await finish(np.concatenate((before, after)))
    async def process_before(self, context):
        """
        Convenience method to process a before group.
        """
        return await self.process(
            context,
            context.before,
            functools.partial(self.sub_before, context),
            functools.partial(self.start_before, context),
            functools.partial(self.finish_before, context)
            )
    async def process_after(self, context):
        """
        Convenience method to process a after group.
        """
        return await self.process(
            context,
            context.after,
            functools.partial(self.sub_after, context),
            functools.partial(self.start_after, context),
            functools.partial(self.finish_after, context)
            )
    async def start(self, context):
        """
        Called at the start of processing, does initialization and such.
        """
        pass
    async def start_before(self, context):
        """
        Called at the start of processing, does initialization and such.
        
        This is used only for a before group,
        override it to get different behaviour.
        """
        await self.start(context)
    async def start_after(self, context):
        """
        Called at the start of processing, does initialization and such.
        
        This is used only for a after group,
        override it to get different behaviour.
        """
        await self.start(context)
    async def sub(self, context, token):
        """
        Get the subhandler which corresponds to this token,
        or None if it does not correspond to any.
        """
        return None
    async def sub_before(self, context, token):
        """
        Get the subhandler which corresponds to this token,
        or None if it does not correspond to any.
        
        This is used only for a before group,
        override it to get different behaviour.
        """
        return await self.sub(context, token)
    async def sub_after(self, context, token):
        """
        Get the subhandler which corresponds to this token,
        or None if it does not correspond to any.
        
        This is used only for a after group,
        override it to get different behaviour.
        """
        return await self.sub(context, token)
    async def finish(self, context, tokens):
        """
        Called at the end of processing, does finalization and such.
        Takes unprocessed tokens.
        Returns all leftovers.
        """
        return tokens
    async def finish_before(self, context, tokens):
        """
        Called at the end of processing, does finalization and such.
        Takes unprocessed tokens.
        Returns all leftovers.
        
        This is used only for a before group,
        override it to get different behaviour.
        It should always return nothing.
        """
        return await self.finish(context, tokens)
    async def finish_after(self, context, tokens):
        """
        Called at the end of processing, does finalization and such.
        Takes unprocessed tokens.
        Returns all leftovers.
        
        This is used only for a after group,
        override it to get different behaviour.
        Anything it returns is up to the parent to process.
        """
        return await self.finish(context, tokens)
    async def __call__(self, context):
        """
        Handle this command.
        
        Needs context.before and context.after
        as array of string.
        The optional context.parent specifies which
        higher handler called this one.
        """
        leftover_before = await self.process_before(context)
        if np.shape(leftover_before) and len(leftover_before):# not empty
            raise ValueError('There were leftovers from the before step:\n'+str(leftover_before))
        leftover_after = await self.process_after(context)
        return leftover_after

def main():
    """
    Does some tests to make sure things work.
    """
    import itertools
    tokens_list = lambda x:list(tokens(x))
    fail = next(iter(itertools.chain(
        filter(lambda x:tokens_list(x[0]) != x[1],[
            ('A', ['A']), # does it handle one token?
            ('A a', ['A', 'a']), # does it split?
            ('Aa B b', ['Aa', 'B', 'b']), # does it handle other lengths?
            ('A"""a Bb', ['A"""a', 'Bb']), # does it handle quotes outside of literals?
            ('"A a" B b', ['A a', 'B', 'b']), # does it handles literals?
            ('"\\"" Aa', ['"', 'Aa']), # does it handle quotes in literals?
            ('"\\\\\\"" Aa', ['\\"', 'Aa']), # does it handle backslashes in literals?
            ('A "" a', ['A', '', 'a']), # does it handle empty strings?
            ('A    a B\nb', ['A', 'a', 'B', 'b']), # does it handle unusual spacing?
            ('A\\t\\r\\na "B\\t\\r\\nb"', ['A\\t\\r\\na', 'B\t\r\nb']), # does it handle newlines in literals?
            ('Aa "\xab\ucdef"', ['Aa', '\xab\ucdef']), # does it handle unicode?
            ('Aa "\\xab\\ucdef"', ['Aa', '\xab\ucdef']), # does it handle unicode literals?
            (
                'Aa'+100*' Bb  \n\r \t\t "\\\\\\"\\\\\\\\\\"\\\\\\\\\\\\\\\\\\"\\0\\1\\134\\r\\n\\x7f\xff\\u01ff\\uffff\\U0010ffff"',
                ['Aa']+100*['Bb', '\\"\\\\"\\\\\\\\"\0\1\134\r\n\x7f\xff\u01ff\uffff\U0010ffff']
            ), # just to make sure we didn't miss anything
            ]),
        [None]
        )))
    print('tokens: '+('pass' if fail is None else 'failed at '+str(fail)))

if __name__ == '__main__':
    main()
