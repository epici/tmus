#!/usr/bin/env python3
"""
Tmus - Discord bot for Fantastic Contraption competitions
"""

# import builtin libraries
import datetime
import re
import operator
import traceback
import itertools
import functools
import string
import hashlib
import traceback
import collections

# import installed libraries
import numpy as np
import asyncio
import aiohttp
import discord

# import local libraries
import io_utils
import func_utils
import parse_utils
import data_utils
import crypto_utils
import discord_utils
from builtins import str

# context dependent stringify
def stringify(context, obj):
    verbose = context.verbose
    if verbose < 2:
        # make some trims
        if isinstance(obj, datetime.datetime):
            obj = obj.replace(microsecond=0)
        elif isinstance(obj, datetime.timedelta):
            obj = datetime.timedelta(days=obj.days, seconds=obj.seconds)
    return str(obj)

# synonyms dictionaries
synonym = {}
desynonym = {}
def get_synonym(canon):
    """
    Get all synonyms for a word.
    """
    return synonym[canon]
def get_canonical(alias):
    """
    Get the canonical word which has
    this as a synonym.
    Returns the input unchanged
    if it does not match anything.
    """
    last = None
    while last != alias:
        last = alias
        alias = desynonym.get(alias, alias)
    return last
def set_synonym(canon, alias, warn=True):
    """
    Set a single or multiple synonyms for this
    canonical word.
    """
    if not isinstance(alias, collections.Iterable):
        alias = alias,
    canon = get_canonical(canon)
    if canon in synonym:
        iset = synonym[canon]
    else:
        iset = synonym[canon] = set()
    for ialias in alias:
        if ialias in desynonym:
            if warn:
                print('Realiasing '+ialias+' from '+desynonym[ialias]+' to '+canon)
            synonym[desynonym[ialias]].remove(ialias)
        iset.add(ialias)
        desynonym[ialias] = canon

# discord client object
client = globals()['client'] = discord.Client()

# queue logout
logout = False

# privilege levels constant
privilege_levels = data_utils.dummy(
    normal = 0,
    admin = 1,
    )

# verbose levels constant
verbose_levels = data_utils.dummy(
    silenced = 0,
    normal = 1,
    full = 2,
    )
# needed privilege levels for verbose levels
verbose_privilege_levels = {
    verbose_levels.silenced:privilege_levels.normal,
    verbose_levels.normal:privilege_levels.normal,
    verbose_levels.full:privilege_levels.admin,
    }
# highest verbose level for a privilege level
privilege_verbose_levels = {
    privilege_levels.normal:verbose_levels.normal,
    privilege_levels.admin:verbose_levels.full,
    }

# load basic info
# expected values:
me = None
#    me.id as string
#    me.secret as string
#    me.token as string
#    me.prefix as set of string
ids = None
#    ids.bots as set of string
#    ids.admins as set of string
rate_limits = None
#    rate_limits.server as float
#    rate_limits.channel as float
#    rate_limits.user as float
colours = None
#    colours.normal as discord.Colour
#    colours.error as discord.Colour
settings = None
#    settings.login_delay as float to float function
#        on login attempt index n, how long to wait before? (in seconds)
io_utils.load('local/tmus_info.py', _globals=globals())
discord_utils.embed_colours.update(colours.__dict__) # add all colours

# expected values:
#    synonyms loaded via set_synonym
io_utils.load('local/translations.py', _globals=globals())

# expected values:
describe_texts = None
io_utils.load('local/describe_texts.py', _globals=globals())

# create rate limit trackers
last_used = data_utils.dummy()
last_used.server = {}
last_used.channel = {}
last_used.user = {}

async def get_privilege_level(uid):
    return privilege_levels.admin if uid in ids.admins else privilege_levels.normal

async def check_rate(message):
    """
    Check if a message complies with rate limits.
    """
    current_time = datetime.datetime.utcnow()
    checks = [
        [message.channel.id, last_used.channel, rate_limits.channel],
        [message.author.id, last_used.user, rate_limits.user],
        ]
    if message.server:
        # DM has no server
        checks.append([message.server.id, last_used.server, rate_limits.server])
    for uid, lu, rl in checks:
        if uid in lu and (current_time - lu[uid]).total_seconds() < rl:
            return False
    return True

async def update_rate(message):
    """
    Update last_used dict for a message.
    """
    current_time = datetime.datetime.utcnow()
    checks = [
        [message.channel.id, last_used.channel],
        [message.author.id, last_used.user],
        ]
    if message.server:
        # DM has no server
        checks.append([message.server.id, last_used.server])
    for uid, lu in checks:
        lu[uid] = current_time

async def reply_exception(context, exception, details):
    verbose = context.verbose
    if verbose == verbose_levels.silenced:
        return -1 # signal silenced
    elif verbose == verbose_levels.normal:
        return await discord_utils.make_embed(
            colour='error',
            title='Something went wrong',
            description=str(exception),
            )
    elif verbose == verbose_levels.full:
        return await discord_utils.make_embed(
            colour='error',
            title=type(exception).__name__ + ' at ' + str(datetime.datetime.utcnow()),
            description=details
            )

class handler_logout_key(parse_utils.handler):
    def __init__(self, key):
        self.key = key
    async def finish_after(self, context, tokens):
        context.parent.keys.append(self.key)
        return await parse_utils.handler.finish_after(self, context, tokens)

class handler_logout(parse_utils.handler):
    async def start_after(self, context):
        self.accepted_keys = crypto_utils.time_hash(context.now)
        self.keys = []
    async def sub_after(self, context, token):
        if len(token)==6 and re.match('\\d+', token):
            return handler_logout_key(token)
        raise ValueError('Logout command only supports time-based key as parameter.')
    async def finish_after(self, context, tokens):
        global logout
        correct = set(self.accepted_keys) & set(self.keys)
        if correct:
            context.result = -1
            logout = True
        elif context.privilege_level[0] >= privilege_levels.admin:
            context.result = await discord_utils.make_embed(
                colour='normal',
                title='Logout key',
                description='Use this key to logout:\n'+self.accepted_keys[-1]
                )
        else:
            context.result = await discord_utils.make_embed(
                colour='error',
                title='Logout needs admin',
                description='You are not an admin, or you are not running this command as an admin.'
                )
        return await parse_utils.handler.finish_after(self, context, tokens)

class handler_describe(parse_utils.handler):
    async def finish_after(self, context, tokens):
        items = []
        for token in tokens:
            token = get_canonical(token)
            if token not in ('--is','--a'):
                items.append(token)
        if len(items)>1:
            raise ValueError('I found more than one thing, please only ask me to describe one thing at a time.')
        name = items[0] if items else '--this'
        if name not in describe_texts:
            raise ValueError('I do not know what '+repr(name)+' is')
        text = describe_texts[name]
        text = re.sub('\\{([^\\}]+)\\}', lambda x:stringify(context, eval(x.group(1))), text)
        context.result = await discord_utils.make_embed(
            colour='normal'
            )
        context.result.title, context.result.description = text.split('\n---\n')
        return np.array([], dtype=str)

class handler_root_as(parse_utils.handler):
    async def finish_after(self, context, tokens):
        if len(tokens)<1:
            raise ValueError('You need to tell me what level or who to run as.')
        token = tokens[0]
        token = get_canonical(token)
        match = re.findall('\\d{18}', token)
        if match:
            uid = match[0]
            context.parent.context.user = await discord_utils.get_user(uid, client=client, server=context.message.server)
        else:
            if hasattr(privilege_levels, token):
                req = getattr(privilege_levels, token)
            elif re.match('\\d', token):
                req = int(token)
            elif token == '--max':
                req = context.privilege_level[1]
            else:
                raise ValueError('I don\'t know how to run as '+repr(token)+'.')
            if req > context.privilege_level[1]:
                raise ValueError('You can\'t go higher than your current highest privilege level.')
            context.privilege_level[:] = req
        return tokens[1:]

class handler_root_verbose(parse_utils.handler):
    async def finish_after(self, context, tokens):
        if len(tokens)<1:
            raise ValueError('You need to tell me what level of verbosity you want.')
        token = tokens[0]
        token = get_canonical(token)
        if hasattr(verbose_levels, token):
            req = getattr(verbose_levels, token)
        elif re.match('\\d', token):
            req = int(token)
        elif token == '--max':
            req = privilege_verbose_levels[context.privilege_level[1]]
        else:
            raise ValueError('I don\'t know verbosity level '+repr(token)+'.')
        if req > privilege_verbose_levels[context.privilege_level[1]]:
            raise ValueError('You need a higher privilege level to use this verbosity level.')
        context.verbose = req
        return tokens[1:]

class handler_root(parse_utils.handler):
    def __init__(self):
        self.commands = dict(commands)
        self.commands.update({
            '--as':handler_root_as,
            '--verbose':handler_root_verbose,
            })
        self.subbed = False
    async def sub_after(self, context, token):
        if self.subbed:return None
        canon = get_canonical(token)
        if canon in self.commands:
            # rest of the command will be passed on
            self.subbed = canon in commands # if in the global dict
            return self.commands[canon]()
        # TODO for later: id or design link -> submit design
    async def finish(self, context, tokens):
        if len(tokens):# not empty
            raise ValueError('There were leftovers I could not understand:\n'+' '.join(tokens))
        return tokens

# map keywords to handler constructor or function
commands = {
    '--logout': handler_logout,
    '--describe': handler_describe,
    }

@client.event
async def on_ready():
    # notify on login
    print('='*40)
    print('Logged in as '+client.user.name+'#'+str(client.user.discriminator)+'\n\tID '+client.user.id)
    if client.user.id != me.id:
        print('ID mismatch. Info file says '+me.id)
    print('Current time is '+str(datetime.datetime.utcnow()))
    print('='*40)

@client.event
async def on_message(message):
    # rate limits take priority
    if not await check_rate(message):return
    # this will be set later
    embed = None
    # make local variables
    mcontent = message.content
    mauthor = message.author
    # create the context
    context = data_utils.dummy()
    context.message = message
    context.result = None
    context.now = message.timestamp
    context.user = message.author
    context.target = None
    context.verbose = verbose_levels.normal
    content = None
    # look for a matching prefix
    for prefix in me.prefix:
        if mcontent.startswith(prefix):
            content = mcontent[len(prefix):]
            break
    # in DM, can use without prefix but will be silenced by default
    if not content and not message.server:
        content = mcontent
        context.verbose = verbose_levels.silenced
    if content:# possibly a command, so handle it
        try:
            context.before = np.array([], dtype=str)
            context.after = np.array(list(parse_utils.tokens(content.strip())), dtype=str)
            context.privilege_level = np.array([privilege_levels.normal, await get_privilege_level(mauthor.id)], dtype=int)
            handler = handler_root()
            await handler(context)
            embed = context.result
        except Exception as exception:
            details = traceback.format_exc(5)
            embed = await reply_exception(context, exception, details)
        if embed:
            # need to update rate limits
            await update_rate(message)
            # respond if right type
            if isinstance(embed, discord.Embed):
                await client.send_message(context.target or message.channel, embed=embed)
    if logout:
        await client.logout()

def main():
    """
    The main function.
    Runs the bot until told to stop.
    Saves various things to globals so it can be
    inspected after a run.
    """
    event_loop = globals()['event_loop'] = asyncio.get_event_loop()
    try:
        for retries in itertools.count():
            event_loop.run_until_complete(
                asyncio.sleep(settings.login_delay(retries))
                )
            event_loop.run_until_complete(asyncio.gather(
                client.start(me.token)
                ))
            if logout:break
        client.close()
    except Exception as exception:
        print(exception)

if __name__ == '__main__':
    main()
