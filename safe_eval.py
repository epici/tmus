"""
Utilities module for safely evaluating Python code
from untrusted sources.
"""

import ast
import operator
import functools
import numpy as np

def ast_call_wrap(func, node=None):
    """
    Function which spits out a new node
    that is the output of function func
    on the original as input.
    If func is given as a string, it will be converted
    to the corresponding variable get.
    """
    if isinstance(func, str):
        func = ast.Name(id=func, ctx=ast.Load())
    result = ast.Call(
        func = func,
        args = [node],
        keywords = []
        )
    ast.copy_location(result, node)
    ast.fix_missing_locations(result)
    return result

class eval_profile(object):
    """
    Represents an evaluation profile.
    Different profiles may judge safety differently
    and compile or run differently.
    
    This base class is very unspecial.
    Subclasses should make the methods work however is best
    for them, and not copy these.
    Users should know what subclass they are using,
    so this is not a problem.
    """
    def compile(self, source, filename='<string>', mode='eval', *args, **kwargs):
        """
        Attempt to compile some code.
        If compilation fails or if it's deemed unsafe or whatever,
        raise an error.
        Otherwise, return the compiled code object.
        """
        return compile(source, filename, mode, *args, **kwargs)
    def eval(self, expression, _globals=None, _locals=None, _builtins=None):
        """
        Attempt to evaluate some code.
        If evaluation fails, raise an error.
        """
        _globals = dict(_globals or {})
        _globals['__builtins__'] = dict(_builtins or _globals.get('__builtins__', {}))
        return eval(expression, _globals, _locals)

class eval_vectorized(eval_profile, ast.NodeTransformer):
    """
    A family of evaluation profiles
    specifically for vectorized math calculations.
    """
    def __init__(self, _globals=None, int_wrap=None):
        """
        Initialize this profile.
        Builtins will always be the specified.
        Globals will contain the specified,
        with variables given later overriding things.
        Provide a function for wrapping integer literals
        to be more safe. Input is the literal AST, output
        should be a converted AST. Alternatively, provide a string name
        for the converting function.
        """
        self._globals = _globals = dict(_globals or {})
        _globals['__builtins__'] = {}
        self.int_wrap = (functools.partial(ast_call_wrap, int_wrap) if isinstance(int_wrap, str) else int_wrap) if int_wrap else (lambda x:x)
    def compile(self, source, filename='<string>', mode='eval'):
        """
        Compile this code. Vectorized math only.
        """
        tree = ast.parse(source, filename, mode)
        tree = self.visit(tree)
        return compile(tree, filename, mode)
    def eval(self, expression, variables=None):
        """
        Evaluate this code. Should have been checked by compile.
        Provide extra variables in a dictionary.
        """
        _globals = dict(self._globals)
        _globals.update(variables or {})
        return eval(expression, _globals)
    def visit_Num(self, node):
        num = node.n
        if isinstance(num, int):
            node = self.int_wrap(node)
        return node
    def visit_Atrribute(self, node):
        """
        Visit a Attribute type node.
        Called when processing with ast.
        """
        name = node.attr
        if '__' in name:
            raise ValueError('Your code is trying to access an attribute with two underscores. That\'s dangerous, so I won\'t run it.')
        return node
    def visit_Str(self, node):
        """
        Visit a Str type node.
        Called when processing with ast.
        """
        raise ValueError('No strings allowed. You\'re doing math, you don\'t need strings.')
    def visit_JoinedStr(self, node):
        """
        Visit a JoinedStr type node.
        Called when processing with ast.
        """
        raise ValueError('No strings allowed. That means no f-strings as well.')
    def visit_Bytes(self, node):
        """
        Visit a Bytes type node.
        Called when processing with ast.
        """
        raise ValueError('No strings allowed. No bytes either.')
    def visit_For(self, node):
        """
        Visit a For type node.
        Called when processing with ast.
        """
        raise ValueError('No for loops allowed. Use vectorized things instead.')

# This will be a commonly used one
eval_numpy = eval_vectorized(
    _globals = {
        'abs': np.abs,
        'acos': np.arccos,
        'acosh': np.arccosh,
        'add': np.add,
        'all': np.all,
        'any': np.any,
        'arange': np.arange,
        'arccos': np.arccos,
        'arccosh': np.arccosh,
        'arcsin': np.arcsin,
        'arcsinh': np.arcsinh,
        'arctan': np.arctan,
        'arctanh': np.arctanh,
        'array': np.array,
        'asin': np.arcsin,
        'asinh': np.arcsinh,
        'atan': np.arctan,
        'atanh': np.arctanh,
        'ceil': np.ceil,
        'choose': np.choose,
        'complex': np.complex64,
        'convolve': np.convolve,
        'cos': np.cos,
        'cosh': np.cosh,
        'dot': np.tensordot,
        'diagonal': np.diagonal,
        'div': np.divide,
        'divide': np.divide,
        'divmod': np.divmod,
        'e': np.e,
        'exp': np.exp,
        'expand_dims': np.expand_dims,
        'float': np.float64,
        'floor': np.floor,
        'floordiv': np.floor_divide,
        'floor_divide': np.floor_divide,
        'identity': np.identity,
        'imag': np.imag,
        'imaginary': np.imag,
        'inf': np.inf,
        'int': np.int64,
        'len': len,
        'list': np.array,
        'log': np.log,
        'log10': np.log10,
        'log2': np.log2,
        'matmul': np.tensordot,
        'matrix_multiply': np.tensordot,
        'max': np.max,
        'mod': np.remainder,
        'modulo': np.remainder,
        'mul': np.multiply,
        'multiply': np.multiply,
        'min': np.min,
        'minus': np.subtract,
        'nan': np.nan,
        'pi': np.pi,
        'pow': np.power,
        'product': np.product,
        'range': np.arange,
        'real': np.real,
        'remainder': np.remainder,
        'repeat': np.repeat,
        'reverse': operator.itemgetter(slice(None,None,-1)),
        'reversed': operator.itemgetter(slice(None,None,-1)),
        'roll': np.roll,
        'rollaxis': np.rollaxis,
        'round': np.round,
        'select': np.select,
        'shape': np.shape,
        'sin': np.sin,
        'sinh': np.sinh,
        'sort': np.sort,
        'sorted': np.sort,
        'sqrt': np.sqrt,
        'sub': np.subtract,
        'subtract': np.subtract,
        'sum': np.sum,
        'swapaxes': np.swapaxes,
        'tan': np.tan,
        'tanh': np.tanh,
        'tau': np.pi*2,
        'tensordot': np.tensordot,
        'tuple': np.array,
        'turn': np.pi*2,
        'twopi': np.pi*2,
        'uint': np.uint64,
        'where': np.where,
        'zip': np.transpose,
        },
    int_wrap = 'int'
    )
