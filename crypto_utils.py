"""
Utilities for things involving cryptography.
"""

import datetime
import hashlib
import struct

def time_hash(when:datetime=None, interval:int=30, prefix:bytes=b'', suffix:bytes=b'', back:int=1, forward:int=1, digits:int=6):
    """
    Get a list of 256-bit (32 byte) hashes unique to the time.
    Based on SHA3-256 hash.
    
    Args:
        when: if provided, the time to use, otherwise uses the current time
        interval: number of seconds to change
        prefix: message to be hashed will be prefixed by this
        suffix: message to be hashed will be suffixed by this
        back: how many steps backward to also calculate for (endpoint inclusive)
        forward: how many steps forward to also calculate for (endpoint exclusive)
        digits: if specified, returns strings which have this many digits instead of hashes
    
    Returns:
        List of hashes in order from earliest time to latest time, as bytes.
    """
    when = when or datetime.datetime.utcnow()
    num = int(when.timestamp()//interval)
    result = []
    for inum in range(num-back,num+forward):
        val = struct.pack('<L', inum)
        val = prefix + val + suffix
        val = hashlib.sha3_256(val).digest()
        if digits:
            val = int.from_bytes(val, 'little')
            val %= 10**digits
            val = str(val).zfill(digits)
        result.append(val)
    return result