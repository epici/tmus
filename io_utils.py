"""
Makes saving and loading data easier.
"""

class auto_repr(object):
    """
    This class has a premade repr,
    extend it to use it.
    The class variable repr_variables defines what
    instance variables should be saved.
    """
    def __repr__(self):
        selftype = type(self)
        repr_variables = None
        for superclass in selftype.mro():
            if hasattr(superclass, 'repr_variables'):
                repr_variables = superclass.repr_variables
                break
        if not repr_variables:
            repr_variables = sorted(filter(lambda x:not x.startswith('_'),dir(self)))
        parts = []
        for key in repr_variables:
            value = repr(getattr(self, key))
            parts.append(key+'='+value)
        return selftype.__name__ + '(' + ', '.join(parts) + ')'

def load(filename, _globals=None):
    """
    Attempt to load data from a file.
    """
    _globals = _globals or globals()
    try:
        with open(filename,'r',encoding='utf-8') as file:
            data = file.read()
        exec(data, _globals)
    except FileNotFoundError as err:
        print(err)

def save(filename, names, _globals=None):
    """
    Attempt to write the values of the given variables to a file.
    """
    _globals = _globals or globals()
    lines = []
    for name in names:
        try:
            lines.append(name+' = '+repr(_globals[name]))
        except KeyError as err:
            lines.append('#'+name+' missing')
            print(err)
    data = '\n'.join(lines)
    with open(filename,'w',encoding='utf-8') as file:
        file.write(data)

def log(code, filename=None, _globals=None):
    """
    Execute some code, and write the code executed to a file.
    """
    _globals = _globals or globals()
    exec(code, _globals)
    if filename:
        with open(filename,'a',encoding='utf-8') as file:
            file.write('\n'+code)
